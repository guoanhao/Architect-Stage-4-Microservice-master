//package com.imooc.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.filter.CorsFilter;
//
///**
// * 跨域的配置，前端想要访问后端，就要配置跨域
// */
//@Configuration
//public class CorsConfig {
//
//    public CorsConfig() {
//    }
//
//    @Bean
//    public CorsFilter corsFilter() {
//        // 1.添加cors配置信息
//        CorsConfiguration config = new CorsConfiguration();
//        // 添加前端请求
//        // 本机前端路径
//        config.addAllowedOrigin("http://localhost:8080");
//        config.addAllowedOrigin("http://shop.ga.com:8080");
//        // 生产环境前端了路径
//        config.addAllowedOrigin("http://192.168.40.135:8080");
//        // 生产环境ip
//        config.addAllowedOrigin("http://192.168.40.135");
//        // nginx前端路径
//        config.addAllowedOrigin("http://192.168.40.130:90");
//        // nginx服务器的ip
//        config.addAllowedOrigin("http://192.168.40.130");
//        // 允许放行任何请求
//        config.addAllowedOrigin("*");
//
//
//        // 设置是否发送cookie信息，前段源码中也有相应的设置：axios.defaults.withCredentials = true;
//        config.setAllowCredentials(true);
//        // 设置允许请求的方式
//        config.addAllowedMethod("*");
//        // 设置允许的header
//        config.addAllowedHeader("*");
//
//        // 2.为url添加映射路径
//        UrlBasedCorsConfigurationSource corsSource = new UrlBasedCorsConfigurationSource();
//        corsSource.registerCorsConfiguration("/**", config);
//
//        // 3.返回重新定义好的corsSource
//        return new CorsFilter(corsSource);
//
//    }
//}
