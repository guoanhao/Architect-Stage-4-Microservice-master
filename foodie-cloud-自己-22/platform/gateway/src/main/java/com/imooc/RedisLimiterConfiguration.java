package com.imooc;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import reactor.core.publisher.Mono;

/**
 * Created by 半仙.
 * 限流配置类
 */
@Configuration
public class RedisLimiterConfiguration {

    // HostAddress的Key
    @Bean
    @Primary
    public KeyResolver remoteAddrKeyResolver() {
        return exchange -> Mono.just(
                exchange.getRequest()
                        .getRemoteAddress()
                        .getAddress()
                        .getHostAddress()
        );
    }

    /**
     * redis限流规则，给用户限流
     * @return
     */
    @Bean("redisLimiterUser")
    @Primary
    public RedisRateLimiter redisLimiterUser() {
        // 限流10，令牌桶20
        return new RedisRateLimiter(10, 20);
    }

    /**
     * redis限流规则，给商品限流
     * @return
     */
    @Bean("redisLimiterItem")
    public RedisRateLimiter redisLimiterItem() {
        // 限流20，令牌桶50
        return new RedisRateLimiter(20, 50);
    }

    // TODO 尝试实现一个in-memory限流器（内存级别的限流）

}
