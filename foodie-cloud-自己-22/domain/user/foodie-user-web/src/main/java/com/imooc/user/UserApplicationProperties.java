package com.imooc.user;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by 半仙.
 * 专门的配置类
 * 用来管理外部注入的属性（从gitee上拉取下来的配置文件）
 * 控制注册功能的开启或关闭（在运行过程中不断刷新）
 */
@Configuration
@RefreshScope
@Data
public class UserApplicationProperties {

    /**
     * gitee上foodie-user-service-dev.yml文件中的以下内容
     * # 功能开关 - 开启/关闭注册功能
     * userservice:
     *   registration:
     *     disabled: true
     */
    @Value("${userservice.registration.disabled}")
    private boolean disableRegistration;

}
