package com.imooc.springcloud.hystrix;

import com.imooc.springcloud.Friend;
import com.imooc.springcloud.MyService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by 半仙.
 * Fallback是MyService的容错类
 */
@Slf4j
@Component
public class Fallback implements MyService {

    @Override
    // 再次发生异常，会去寻求fallback2的帮助
    @HystrixCommand(fallbackMethod = "fallback2")
    public String error() {
        log.info("Fallback: I'm not a black sheep any more");
//        return "Fallback: I'm not a black sheep any more";
        throw new RuntimeException("first fallback");// 抛出异常，再次降级
    }

    // 第二次次降级，再次发生异常，会去寻求fallback3的帮助
    @HystrixCommand(fallbackMethod = "fallback3")
    public String fallback2() {
        log.info("fallback again");
        throw new RuntimeException("fallback again");
    }
    // 第三次次降级
    public String fallback3() {
        log.info("fallback again and again");
        return "success";
    }


    @Override
    public String sayHi() {
        return null;
    }

    @Override
    public Friend sayHiPost(@RequestBody Friend friend) {
        return null;
    }

    @Override
    public String retry(@RequestParam(name = "timeout") int timeout) {
        return "You are late !";
    }

}
