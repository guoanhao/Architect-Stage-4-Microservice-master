package com.imooc.springcloud;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 半仙.
 */
@RestController
public class Controller {

    // gitee上配置文件中的name，直接从外部的配置文件中加载
    @Value("${name}")
    private String name;

    // 将外部的属性注入到自己项目的配置文件，再从配置文件中加载。配置文件中有体现
    @Value("${myWords}")
    private String words;

    @GetMapping("/name")
    public String getName() {
        return name;
    }

    @GetMapping("/words")
    public String getWords() {
        return words;
    }

}
