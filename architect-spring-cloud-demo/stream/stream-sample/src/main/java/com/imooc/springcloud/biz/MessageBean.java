package com.imooc.springcloud.biz;

import lombok.Data;

/**
 * Created by 半仙.
 */
@Data
public class MessageBean {

    // 消息体
    private String payload;

}
