package com.imooc.springcloud.自定义广播消息;

import org.springframework.cloud.bus.event.RemoteApplicationEvent;

public class MyEvent extends RemoteApplicationEvent {

    public MyEvent() {
    }

    public MyEvent(Object body, String originService, String destinationService) {
        super(body, originService, destinationService);
    }

}
