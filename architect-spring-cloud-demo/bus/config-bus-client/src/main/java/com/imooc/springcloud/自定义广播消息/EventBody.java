package com.imooc.springcloud.自定义广播消息;

import lombok.Data;

@Data
public class EventBody {

    public String name;

    public int age;
}
