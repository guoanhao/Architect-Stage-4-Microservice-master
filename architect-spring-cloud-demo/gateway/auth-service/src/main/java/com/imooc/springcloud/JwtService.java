package com.imooc.springcloud;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by 半仙.
 */
@Slf4j
@Service
public class JwtService {

    // 生产环境不能这么用，要进行加密后，放到外部（比如gitee）
    private static final String KEY = "changeIt";
    private static final String ISSUER = "yao";


    private static final long TOKEN_EXP_TIME = 60000;
    private static final String USER_NAME = "username";

    /**
     * 生成Token
     *
     * @param acct
     * @return
     */
    public String token(Account acct) {
        Date now = new Date();
        // 生成token用到的算法
        Algorithm algorithm = Algorithm.HMAC256(KEY);

        String token = JWT.create()
                .withIssuer(ISSUER)
                .withIssuedAt(now)
                // 过期时间（token生成时间 + 过期时间）
                .withExpiresAt(new Date(now.getTime() + TOKEN_EXP_TIME))
                // 添加一些自定义的属性
                .withClaim(USER_NAME, acct.getUsername())
//                .withClaim("ROLE", "")
                // 签发token
                .sign(algorithm);

        log.info("jwt generated user={}", acct.getUsername());
        return token;
    }

    /**
     * 校验Token
     *
     * @param token
     * @param username
     * @return
     */
    public boolean verify(String token, String username) {
        log.info("verifying jwt - username={}", username);

        try {
            Algorithm algorithm = Algorithm.HMAC256(KEY);
            // 验证函数
            JWTVerifier verifier = JWT.require(algorithm)
                    // 验证发行者
                    .withIssuer(ISSUER)
                    .withClaim(USER_NAME, username)
                    .build();

            // 验证
            verifier.verify(token);
            return true;
        } catch (Exception e) {
            log.error("auth failed", e);
            return false;
        }

    }

}
