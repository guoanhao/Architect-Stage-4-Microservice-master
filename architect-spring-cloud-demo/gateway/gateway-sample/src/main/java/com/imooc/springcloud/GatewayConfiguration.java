package com.imooc.springcloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;

import java.time.ZonedDateTime;

/**
 * Created by 半仙.
 * 在java中配置路由规则，还有一种方式是在yml中配置
 */
@Configuration
public class GatewayConfiguration {

    // 计时接口，注入进来
    @Autowired
    private TimerFilter timerFilter;

    // 权限验证
    @Autowired
    private AuthFilter authFilter;

    /**
     * 地址定位的方法
     * @param builder
     * @return
     */
    @Bean
    @Order
    public RouteLocator customizedRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/java/**")
                    .and().method(HttpMethod.GET)
                    .and().header("name")
                    // filters 是一个数组
                    .filters(f -> f.stripPrefix(1)
                            .addResponseHeader("java-param", "gateway-config")
                            // 添加计时规则
                            .filter(timerFilter)
                            // 权限验证
                            .filter(authFilter)
                    )
                    .uri("lb://FEIGN-CLIENT") // 负载均衡
                )
                // 路径满足这个条件之后，跳转到FEIGN-CLIENT
                .route(r -> r.path("/seckill/**")
                        // after 是在某个时间点之后才生效
                        .and().after(ZonedDateTime.now().plusMinutes(1))
                        // before 在某个时间点之前生效，过期不候
//                        .and().before()
                        // 在两个时间点之间的时间生效
//                        .and().between()
                        // 删除r.path("/seckill/**")中的seckill
                        .filters(f -> f.stripPrefix(1))
                        .uri("lb://FEIGN-CLIENT")
                )
                .build();

    }

}
