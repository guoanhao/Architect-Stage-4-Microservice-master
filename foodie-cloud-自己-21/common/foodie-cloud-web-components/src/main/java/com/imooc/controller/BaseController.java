package com.imooc.controller;

import org.springframework.stereotype.Controller;

import java.io.File;

@Controller
public class BaseController {

    //pageSize默认为10条
    public static final Integer COMMON_PAGE_SIZE = 10;
    //pageSize默认为20条
    public static final Integer PAGE_SIZE = 20;

    public static final String FOODIE_SHOPCART = "shopcart";

    // 支付中心的调用地址
    public String paymentUrl = "http://payment.t.mukewang.com/foodie-payment/payment/createMerchantOrder";        // produce

    // 微信支付成功 -> 支付中心 -> 天天吃货平台
    //                                      | -> 回调通知的url
//    String payReturnUrl = "http://localhost:8088/orders/notifyMerchantOrderPaid";//这样写有问题，云端的代码访问不到localhost
    public String payReturnUrl = "http://vch3gv.natappfree.cc/orders/notifyMerchantOrderPaid";    //将自己的电脑的ip放到公网上，netapp关闭之后得重新开

    //  http://vch3gv.natappfree.cc

    // 用户上传头像的位置，这里用不到了，该路径在file-upload-dev.properties中配置好了
    public static final String IMAGE_USER_FACE_LOCATION = File.separator + "workspaces" +
            File.separator + "images" +
            File.separator + "foodie" +
            File.separator + "faces";
//    public static final String IMAGE_USER_FACE_LOCATION = "/workspaces/images/foodie/faces"; 斜杠不太标准，改为上面的形式


    // FIXME 下面的逻辑移植到订单中心
//    @Autowired
//    private MyOrdersService myOrdersService;
//    /**
//     * 用于验证用户和订单是否有关联关系，避免非法用户调用
//     * @return
//     */
//    public IMOOCJSONResult checkUserOrder(String userId, String orderId) {
//        Orders order = myOrdersService.queryMyOrder(userId, orderId);
//        if (order == null) {
//            return IMOOCJSONResult.errorMsg("订单不存在！");
//        }
//        return IMOOCJSONResult.ok(order);
//    }

}
