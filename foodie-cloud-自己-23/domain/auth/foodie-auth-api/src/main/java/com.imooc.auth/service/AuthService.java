package com.imooc.auth.service;

import com.imooc.auth.service.pojo.Account;
import com.imooc.auth.service.pojo.AuthResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 半仙.
 * 鉴权服务
 */
@FeignClient("foodie-auth-service")
@RequestMapping("auth-service")
public interface AuthService {

    /**
     * 生成token
     * @param userId
     * @return
     */
    @PostMapping("token")
    public AuthResponse tokenize(@RequestParam("userId") String userId);

    /**
     * 校验token
     * @param account
     * @return
     */
    @PostMapping("verify")
    public AuthResponse verify(@RequestBody Account account);

    /**
     * 刷新token
     * @param refresh
     * @return
     */
    @PostMapping("refresh")
    public AuthResponse refresh(@RequestParam("refresh") String refresh);

    /**
     * 删除token
     * @param account
     * @return
     */
    @DeleteMapping("delete")
    public AuthResponse delete(@RequestBody Account account);

}
